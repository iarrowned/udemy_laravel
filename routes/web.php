<?php

use App\Http\Controllers\TaskController;
use App\Http\Controllers\TestController;
use App\Http\Controllers\UserController;
use App\Http\Middleware\EnsureTokenIsValid;
use App\Models\Address;
use App\Models\Comments;
use App\Models\Role;
use App\Models\Task;
use App\Models\User;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/insert', function () {
    $user = User::findOrFail(1);
    $address = new Address(['name' => '1234 Houston av New York']);

    $user->address()->save($address);
});

Route::get('/update', function () {
    $address = Address::whereUserId(1)->first();
    $address->name = 'new adress name';
    $address->save();
});

Route::get('tasks/{id}', [TaskController::class, 'show']);

Route::get('users/{id}', [UserController::class, 'show']);

Route::get('roles/{id}', function ($id) {
    $role = Role::find($id);
    foreach ($role->users as $user) {
        echo $user->name . PHP_EOL;
    }
});

Route::get('test', [TestController::class, 'index']);
Route::get('test/access', [TestController::class, 'show'])->middleware(EnsureTokenIsValid::class);
