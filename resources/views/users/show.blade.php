@extends('layouts.app')
@section('title', 'User')
@section('content')
    <h1>{{ $user->name }}</h1>
    @if(count($roles) !== 0)
        <h2>Roles</h2>
        <div class="roles">
            <ul>
                @foreach($roles as $role)
                    <li>{{ $role->name }}</li>
                @endforeach
            </ul>
        </div>
    @endif
@endsection
