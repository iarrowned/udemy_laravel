@extends('layouts.app')
@section('title', 'Task')
@section('content')
    <h1>{{ $task->name }}</h1>
    <p>{{ $task->body }}</p>
    @if($comments)
        <h2>Комментарии</h2>
        <div class="comments">
            <ul>
                @foreach($comments as $comment)
                    <li>{{ $comment->body }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if($lastComment)
        <h2>Last comment</h2>
        <p>{{ $lastComment->body }}</p>
    @endif
@endsection
