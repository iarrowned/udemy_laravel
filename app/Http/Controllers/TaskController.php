<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function show($id)
    {
        $task = Task::findOrFail($id);
        $comments = $task->comments;
        $lastComment = $task->latestComment;

        return view('tasks.show', compact('task', 'comments', 'lastComment'));
    }
}
